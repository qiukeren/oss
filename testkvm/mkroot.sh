#!/bin/bash

### Parse command line arguments. Clear and set up environment variables.

# Show usage for any unknown argument, ala "./mkroot.sh --help"
if [ "${1:0:1}" == '-' ] && [ "$1" != '-n' ] && [ "$1" != '-d' ] && [ "$1" != "-l" ]
then
  echo "usage: $0 [-n] [VAR=VALUE...] [MODULE...]"
  echo
  echo Create root filesystem in '$ROOT'
  echo
  echo "-n	Don't rebuild "'$ROOT, just build module(s) over it'
  echo "-d	Don't build, just download/verify source packages."
  echo "-l	Log every command run during build in cmdlog.txt"

  exit 1
fi

# Clear environment variables by restarting script w/bare minimum passed through
[ -z "$NOCLEAR" ] &&
  exec env -i NOCLEAR=1 HOME="$HOME" PATH="$PATH" $(env | grep -i _proxy=) \
    CROSS_COMPILE="$CROSS_COMPILE" CROSS_SHORT="$CROSS_SHORT" "$0" "$@"

# Loop collecting initial -x arguments. (Simple, can't collate ala -nl .)
while true
do
  [ "$1" == "-n" ] && N=1 && shift ||
  [ "$1" == "-d" ] && D=1 && shift ||
  [ "$1" == "-l" ] && WRAPDIR=wrap && shift || break
done

# Parse remaining args: assign NAME=VALUE to env vars, collect rest in $MODULES
while [ $# -ne 0 ]
do
  X="${1/=*/}"
  Y="${1#*=}"
  [ "${1/=/}" != "$1" ] && eval "export $X=\"\$Y\"" || MODULES="$MODULES $1"
  shift
done

# If we're cross compiling, set appropriate environment variables.
if [ -z "$CROSS_COMPILE" ]
then
  echo "Building natively"
  if ! cc --static -xc - -o /dev/null <<< "int main(void) {return 0;}"
  then
    echo "Warning: host compiler can't create static binaries." >&2
    sleep 3
  fi
else
  echo "Cross compiling"
  CROSS_PATH="$(dirname "$(which "${CROSS_COMPILE}cc")")"
  CROSS_BASE="$(basename "$CROSS_COMPILE")"
  [ -z "$CROSS_SHORT" ] && CROSS_SHORT="${CROSS_BASE/-*/}"
  if [ -z "$CROSS_PATH" ]
  then
    echo "no ${CROSS_COMPILE}cc in path" >&2
    exit 1
  fi
fi

# Work out absolute paths to working dirctories (can override on cmdline)
TOP="$PWD"
[ -z "$BUILD" ] && BUILD="$TOP/build"
[ -z "$DOWNLOAD" ] && DOWNLOAD="$TOP/download"
[ -z "$AIRLOCK" ] && AIRLOCK="$TOP/airlock"
[ -z "$OUTPUT" ] && OUTPUT="$TOP/output/${CROSS_SHORT:-host}"
[ -z "$ROOT" ] && ROOT="$OUTPUT/${CROSS_BASE}root"

[ -z "$N" ] && rm -rf "$ROOT"
MYBUILD="$BUILD/${CROSS_BASE:-host-}tmp"
mkdir -p "$MYBUILD" "$DOWNLOAD" || exit 1

### Functions to download, extract, and clean up after source packages.

# This is basically "wget $2"
download()
{
  # Grab source package from URL, confirming SHA1 hash.
  # You can stick extracted source in $DOWNLOAD and build will use that instead
  # Usage: download HASH URL

  FILE="$(basename "$2")"
  [ -d "$DOWNLOAD/${FILE/-*/}" ] && echo "$FILE" local && return 0

  X=0
  while true
  do
    [ "$(sha1sum "$DOWNLOAD/$FILE" 2>/dev/null | awk '{print $1}')" == "$1" ] &&
      echo "$FILE" confirmed &&
      break
    rm -f $DOWNLOAD/${FILE/-[0-9]*/}-[0-9]*
    [ $X -eq 1 ] && break
    X=1
    wget "$2" -O "$DOWNLOAD/$FILE"
  done
}

# This is basically "tar xvzCf $MYBUILD $DOWNLOAD/$1.tar.gz && cd $NEWDIR"
setupfor()
{
  # Extract source tarball (or snapshot a repo) to create disposable build dir.
  # Usage: setupfor PACKAGE

  PACKAGE="$(basename "$1")"
  echo === "$PACKAGE"

  cd "$MYBUILD" &&
  if [ $(ls "$MYBUILD" |grep "$PACKAGE"-) ]
  then
    echo "skip existing package"
    cd "$PACKAGE"-* || exit 1
  else
    #tar xvaf "$DOWNLOAD/$PACKAGE"-*.tar.* > /dev/null &&
    tar xvaf "$DOWNLOAD/$PACKAGE"-*.tar.* &&
    cd "$PACKAGE"-* || exit 1
  fi
}

# This is basically "rm -rf $NEWDIR" (remembered from setupfor)
cleanup()
{
  # Delete directory most recent setupfor created, or exit if build failed
  # Usage: cleanup

  [ $? -ne 0 ] && exit 1
  [ -z "$PACKAGE" ] && exit 1
  [ ! -z "$NO_CLEANUP" ] && return
  cd .. && rm -rf "$PACKAGE"* || exit 1
}


# -n skips rebuilding base system, adds to existing $ROOT
if [ ! -z "$N" ]
then
  if [ ! -d "$ROOT" ] || [ -z "$MODULES" ]
  then
    echo "-n needs an existing $ROOT and build files"
    exit 1
  fi

# -d skips everything but downloading packages
elif [ -z "$D" ]
then

### Create files and directories

rm -rf "$ROOT" &&
mkdir -p "$ROOT"/{etc,tmp,proc,sys,dev,home,mnt,root,usr/{bin,sbin,lib},var} &&
chmod a+rwxt "$ROOT"/tmp &&
ln -s usr/bin "$ROOT/bin" &&
ln -s usr/sbin "$ROOT/sbin" &&
ln -s usr/lib "$ROOT/lib" &&

cat > "$ROOT"/init << 'EOF' &&
#!/bin/sh

export HOME=/home
export PATH=/bin:/sbin

mountpoint -q proc || mount -t proc proc proc
mountpoint -q sys || mount -t sysfs sys sys
if ! mountpoint -q dev
then
  mount -t devtmpfs dev dev || mdev -s
  mkdir -p dev/pts
  mountpoint -q dev/pts || mount -t devpts dev/pts dev/pts
fi

if [ $$ -eq 1 ]
then
  # Don't allow deferred initialization to crap messages over the shell prompt
  echo 3 3 > /proc/sys/kernel/printk

  # Setup networking for QEMU (needs /proc)
  ifconfig eth0 10.0.2.15
  route add default gw 10.0.2.2
  [ "$(date +%s)" -lt 1000 ] && rdate 10.0.2.2 # or time-b.nist.gov
  [ "$(date +%s)" -lt 10000000 ] && ntpd -nq -p north-america.pool.ntp.org

  [ -z "$CONSOLE" ] &&
    CONSOLE="$(sed -rn 's@(.* |^)console=(/dev/)*([[:alnum:]]*).*@\3@p' /proc/cmdline)"

  [ -z "$HANDOFF" ] && HANDOFF=/bin/sh && echo Type exit when done.
  [ -z "$CONSOLE" ] && CONSOLE=console
  #exec /sbin/oneit -c /dev/"$CONSOLE" $HANDOFF
  /bin/sh
else
  /bin/sh
  umount /dev/pts /dev /sys /proc
fi
EOF
chmod +x "$ROOT"/init &&

cat > "$ROOT"/etc/passwd << 'EOF' &&
root::0:0:root:/root:/bin/sh
guest:x:500:500:guest:/home/guest:/bin/sh
nobody:x:65534:65534:nobody:/proc/self:/dev/null
EOF

cat > "$ROOT"/etc/group << 'EOF' &&
root:x:0:
guest:x:500:
EOF

echo "nameserver 8.8.8.8" > "$ROOT"/etc/resolv.conf || exit 1

# todo: eliminate busybox

fi # skipped by -n or -d

### Build modules listed on command line

for STAGE_NAME in $MODULES
do
  cd "$TOP" || exit 1
  if [ -z "$D" ]
  then
    . module/"$STAGE_NAME" || exit 1
  else
    eval "$(sed -n '/^download[^(]/{/\\$/b a;b b;:a;N;:b;p}' module/"$STAGE_NAME")"
  fi
done

# Remove build directory if it's empty.
#rmdir "$MYBUILD" "$BUILD" 2>/dev/null
exit 0
