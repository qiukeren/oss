package main

import (
	"github.com/reeflective/readline"
	log "github.com/sirupsen/logrus"
	"golang.org/x/term"
	"os"
	"oss/libs/libshell"
	"strings"
	"syscall"
	"unsafe"
)

type winSize struct {
	row, col       uint16
	xpixel, ypixel uint16
}

func main() {
	libshell.LoadHistory()
	{
		fd := int(os.Stdout.Fd())
		x, y, err := term.GetSize(fd)
		log.Println(x, y, err)
	}
	{
		fd := int(os.Stderr.Fd())
		x, y, err := term.GetSize(fd)
		log.Println(x, y, err)
	}
	{
		fd := int(os.Stdin.Fd())
		x, y, err := term.GetSize(fd)
		log.Println(x, y, err)
	}
	{
		var ws winSize
		ok, _, _ := syscall.Syscall(syscall.SYS_IOCTL, uintptr(syscall.Stdout),
			syscall.TIOCGWINSZ, uintptr(unsafe.Pointer(&ws)))
		log.Println(ok)
		log.Println(ws)
	}
	{
		var ws winSize
		ok, _, _ := syscall.Syscall(syscall.SYS_IOCTL, uintptr(syscall.Stderr),
			syscall.TIOCGWINSZ, uintptr(unsafe.Pointer(&ws)))
		log.Println(ok)
		log.Println(ws)
	}
	{
		var ws winSize
		ok, _, _ := syscall.Syscall(syscall.SYS_IOCTL, uintptr(syscall.Stdin),
			syscall.TIOCGWINSZ, uintptr(unsafe.Pointer(&ws)))
		log.Println(ok)
		log.Println(ws)
	}

	// Create a new shell with a custom prompt.
	rl := readline.NewShell()
	rl.Prompt.Primary(func() string { return "> " })
	rl.Completer = func(line []rune, cursor int) readline.Completions {
		var c = []string{}
		for n, _ := range libshell.Builtin {
			if strings.HasPrefix(n, strings.ToLower(string(line))) {
				c = append(c, n)
			}
		}
		return readline.CompleteValues(c...)
	}

	// Display the prompt, read user input.
	for {
		line, err := rl.Readline()
		if err != nil {
			break
		}
		log.Println(line)
		command := strings.TrimSpace(line)
		if command == "" {
			continue
		}
		libshell.SaveHistory(command)
		if libshell.Builtin[command] != nil {
			var f = libshell.Builtin[command]
			f(command)
		}
	}
}
