package main

import (
	log "github.com/sirupsen/logrus"
	"github.com/vishvananda/netlink"
	"net"
)

func main() {
	eth0, _ := netlink.LinkByName("eth0")
	lo, _ := netlink.LinkByName("lo")
	addr, _ := netlink.ParseAddr("10.0.2.15/24")

	netlink.LinkSetUp(eth0)
	netlink.LinkSetUp(lo)
	netlink.AddrAdd(eth0, addr)

	{
		err := netlink.RouteAdd(&netlink.Route{
			Gw: net.ParseIP("10.0.2.2"),
		})
		if err != nil {
			log.Println(err)
		}
	}
}
