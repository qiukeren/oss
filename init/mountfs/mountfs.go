package mountfs

import (
	log "github.com/sirupsen/logrus"
	"oss/init/models"
	"syscall"
)

func Load(units []models.Unit) {
	for _, unit := range units {
		if unit.Mount != nil {
			continue
		}
		mount := unit.Mount
		log.Printf("MOUNT source %s target %s fstype %s flags %d data %s", mount.Source, mount.Target, mount.FsType, 0, mount.Data)
		var err error
		err = syscall.Mount(mount.Source, mount.Target, mount.FsType, 0, mount.Data)
		if err != nil {
			log.Println(err)
		}
	}
}

func Init() {
	var err error
	err = syscall.Mount("proc", "proc", "proc", 0, "")
	if err != nil {
		log.Println(err)
	}
	err = syscall.Mount("sys", "sys", "sysfs", 0, "")
	if err != nil {
		log.Println(err)
	}
	err = syscall.Mount("dev", "dev", "devtmpfs", 0, "")
	if err != nil {
		log.Println(err)
	}
}
