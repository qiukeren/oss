package service

import (
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"os"
	"oss/init/models"
	"path/filepath"
	"strings"

	"github.com/go-yaml/yaml"
)

func Load() ([]models.Unit, error) {
	us := []models.Unit{}
	err := filepath.Walk("/planx/etc/services", func(pathStr string, info os.FileInfo, err error) error {
		log.Debugf("Detecting file %s", pathStr)
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}

		if strings.HasSuffix(pathStr, "yaml") {
			return nil
		}

		c, err := ioutil.ReadFile(pathStr)
		if err != nil {
			return err
		}
		u := models.Unit{}
		err = yaml.Unmarshal(c, &u)
		if err != nil {
			return err
		}
		us = append(us, u)

		return nil
	})
	if err != nil {
		return nil, err
	}
	return us, nil
}
