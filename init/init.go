package main

import (
	"bufio"
	"fmt"
	log "github.com/sirupsen/logrus"
	"os"
	"os/exec"
	"oss/init/mountfs"
	"oss/init/reaper"
	"oss/init/service"
	"oss/libs/libnetwork"
	"strconv"
	"strings"
)

func main() {
	//log.SetFlags(log.LstdFlags | log.Lshortfile)

	fmt.Println(`
 __        __    __  __    __   ______    ______    ______  
/  |      /  |  /  |/  |  /  | /      \  /      \  /      \ 
$$ |      $$ |  $$ |$$ |  $$ |/$$$$$$  |/$$$$$$  |/$$$$$$  |
$$ |      $$  \/$$/ $$  \/$$/ $$ |  $$ |$$ |  $$ |$$ \__$$/ 
$$ |       $$  $$<   $$  $$<  $$ |  $$ |$$ |  $$ |$$      \ 
$$ |        $$$$  \   $$$$  \ $$ |  $$ |$$ |  $$ | $$$$$$  |
$$ |_____  $$ /$$  | $$ /$$  |$$ \__$$ |$$ \__$$ |/  \__$$ |
$$       |$$ |  $$ |$$ |  $$ |$$    $$/ $$    $$/ $$    $$/ 
$$$$$$$$/ $$/   $$/ $$/   $$/  $$$$$$/   $$$$$$/   $$$$$$/

`)

	mountfs.Init()

	units, err := service.Load()
	if err != nil {
		log.Println(err)
	}
	log.Debugf("%#v", units)

	mountfs.Load(units)

	mem := ReadMemoryStats()
	log.Printf("Total %dMB Used %dMB", mem.MemTotal, (mem.MemTotal-mem.MemFree)/1024)

	reaper.ReapZombie()

	list := libnetwork.LinkList()
	for _, v := range list {
		log.Println(v.HardwareAddr, v.Ip)
	}

	cmd := exec.Command("/bin/sh")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	cmd.Run()
}

type Memory struct {
	MemTotal     int
	MemFree      int
	MemAvailable int
}

func ReadMemoryStats() Memory {
	file, err := os.Open("/proc/meminfo")
	if err != nil {
		panic(err)
	}
	defer file.Close()
	bufio.NewScanner(file)
	scanner := bufio.NewScanner(file)
	res := Memory{}
	for scanner.Scan() {
		key, value := parseLine(scanner.Text())
		switch key {
		case "MemTotal":
			res.MemTotal = value
		case "MemFree":
			res.MemFree = value
		case "MemAvailable":
			res.MemAvailable = value
		}
	}
	return res
}

func parseLine(raw string) (key string, value int) {
	text := strings.ReplaceAll(raw[:len(raw)-2], " ", "")
	keyValue := strings.Split(text, ":")
	return keyValue[0], toInt(keyValue[1])
}

func toInt(raw string) int {
	if raw == "" {
		return 0
	}
	res, err := strconv.Atoi(raw)
	if err != nil {
		panic(err)
	}
	return res
}
