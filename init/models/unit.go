package models

type Unit struct {
	Description   string
	Documentation string
	Wants         string
	After         string
	Requires      string
	Before        string

	Service *Service
	Mount   *Mount
}

type Service struct {
	Environment map[string]string
	//Restart     string
	//TimeoutStopSec int
	//ExecStartPre   []string
	ExecStart string
	//ExecStop       string
	//ExecStopPost   []string
	PIDFile string
	Type    ServiceType
}

type ServiceType string

const (
	Once   ServiceType = "once"
	Simple ServiceType = "simple"
)

type Mount struct {
	Source string
	Target string
	FsType string
	Data   string
}
