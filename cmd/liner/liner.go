package main

import (
	"github.com/peterh/liner"
	log "github.com/sirupsen/logrus"
	"io"
	"oss/libs/libshell"
	"strings"
)

func main() {

	line := liner.NewLiner()
	defer line.Close()

	line.SetCtrlCAborts(true)

	var compFunc = func(line string) (c []string) {
		for n, _ := range libshell.Builtin {
			if strings.HasPrefix(n, strings.ToLower(line)) {
				c = append(c, n)
			}
		}
		return
	}

	line.SetCompleter(compFunc)

	historyes := libshell.LoadHistory()
	for _, v := range historyes {
		line.AppendHistory(v)
	}

REPL:
	for {
		if command, err := line.Prompt(">> "); err == nil {
			if strings.TrimSpace(command) == "" {
				continue
			}
			if strings.TrimSpace(command) == "exit" {
				break REPL
			}
			if libshell.Builtin[command] != nil {
				var f = libshell.Builtin[command]
				f(command)
			}
			line.AppendHistory(command)
			libshell.SaveHistory(command)
		} else if err == liner.ErrPromptAborted || err == io.EOF {
			log.Print("Exited")
			break REPL
		} else {
			log.Print("Error reading line: ", err)
		}
	}

}
