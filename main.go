package main

import (
	"log"

	"github.com/vishvananda/netlink"
	"github.com/vishvananda/netlink/nl"
)

func main() {

	lo, _ := netlink.LinkByName("eth0")
	log.Printf("%#v", lo.Attrs().HardwareAddr.String())
	log.Println(lo.Type())
	a, err := netlink.AddrList(lo, nl.FAMILY_V4)
	log.Println(err)
	log.Println(a)
}
