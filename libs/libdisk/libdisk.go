package libdisk

import (
	"github.com/pkg/errors"
	"io/ioutil"
	"log"
	"os"
)

func ListDisk() ([]string, error) {
	if _, err := os.Stat("/sys/block"); !os.IsNotExist(err) {
		blockDevs, err := ioutil.ReadDir("/sys/block")
		if err != nil {
			log.Println("ReadDir /sys/block error: %s", err)
			return nil, errors.Wrap(err, "ioutil.ReadDir(/sys/block)")
		}
		var devices = []string{}
		for _, v := range blockDevs {
			devices = append(devices, v.Name())
		}
		return devices, nil
	} else {
		return []string{}, errors.Wrap(err, "stat /sys/block fail")
	}

}
