package libstorage

import (
	"database/sql"
	log "github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"testing"
	"time"
)

type TestS struct {
	gorm.Model

	Name         string
	Email        *string
	Age          uint8
	Birthday     *time.Time
	MemberNumber sql.NullString
}

func TestNewStorage(t *testing.T) {
	s, err := NewStorage[TestS]("tests.db")
	if err != nil {
		t.Error(err)
	}
	log.Println(s)
	err = s.Migrate()
	if err != nil {
		t.Error(err)
	}
}

func TestStorage_QueryOne(t *testing.T) {
	ts := TestS{
		Model:        gorm.Model{},
		Name:         "",
		Email:        nil,
		Age:          0,
		Birthday:     nil,
		MemberNumber: sql.NullString{},
	}

	s, err := NewStorage[TestS]("tests.db")
	if err != nil {
		t.Error(err)
	}
	s.InsertOne(ts)
	res, err := s.QueryMany("select * from test_s")
	if err != nil {
		t.Error(err)
	}
	log.Println(len(res))
	log.Println(res)
}
