package libstorage

import (
	"github.com/glebarez/sqlite"
	"gorm.io/gorm"
)

type Storage[T any] struct {
	db *gorm.DB
}

func NewStorage[T any](file string) (*Storage[T], error) {
	db, err := gorm.Open(sqlite.Open(file), &gorm.Config{})
	if err != nil {
		return nil, err
	}
	s := &Storage[T]{db: db}
	return s, nil
}

func (s *Storage[T]) Migrate() error {
	var c T
	err := s.db.AutoMigrate(&c)
	return err
}

func (s *Storage[T]) QueryOne(sql string) (T, error) {
	var c T
	err := s.db.Raw(sql).Find(&c).Error
	return c, err
}

func (s *Storage[T]) QueryMany(sql string) ([]T, error) {
	var c []T
	err := s.db.Raw(sql).Find(&c).Error
	return c, err
}

func (s *Storage[T]) InsertOne(t T) error {
	return s.db.Save(&t).Error
}
