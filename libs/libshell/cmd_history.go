package libshell

import (
	"bytes"
	"fmt"
	"github.com/olekukonko/tablewriter"
	"oss/libs/libstorage"
	"strconv"
	"time"
)

func CmdShellHistory(cmd string) {
	history, err := libstorage.NewStorage[ShellHistory](history_db)

	var outBuffer = bytes.Buffer{}
	table := tablewriter.NewWriter(&outBuffer)
	table.SetHeader([]string{"ID", "Time", "User", "Content"})

	if err == nil {
		history.Migrate()
		c, err := history.QueryMany("select * from shell_histories")
		if err == nil {
			for _, v := range c {
				table.Append([]string{
					strconv.Itoa(int(v.ID)), v.CreatedAt.Format(time.DateTime), v.User, v.Content,
				})
			}
		}
	}

	table.Render()
	fmt.Print(outBuffer.String())
}
