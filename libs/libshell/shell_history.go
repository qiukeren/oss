package libshell

import (
	"oss/libs/liblock"
	"oss/libs/libstorage"
)

func LoadHistory() []string {
	cmds := []string{}
	history, err := libstorage.NewStorage[ShellHistory](history_db)
	if err == nil {
		history.Migrate()
		c, err := history.QueryMany("select * from shell_histories")
		if err == nil {
			for _, v := range c {
				cmds = append(cmds, v.Content)
			}
		}
	}
	return cmds
}

func SaveHistory(content string) {
	sh := ShellHistory{
		Content: content,
	}
	liblock.Lock(history_lock)
	history, err := libstorage.NewStorage[ShellHistory](history_db)
	if err == nil {
		history.InsertOne(sh)
	}
	liblock.UnLock(history_lock)
}
