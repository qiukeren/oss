package libshell

import "oss/libs/libcoreutil"

var Builtin = map[string]func(string){
	"history": CmdShellHistory,
	"sh":      CmdShellBash,
	"bash":    CmdShellBash,
	"env":     libcoreutil.CoreEnv,
	"date":    libcoreutil.CoreDate,
	"whoami":  libcoreutil.CoreUser,
	"who":     libcoreutil.CoreUser,
}
