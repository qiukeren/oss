package libshell

import (
	"os"
	"os/exec"
)

func CmdShellBash(cmdStr string) {
	cmd := exec.Command("/bin/sh")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	cmd.Run()
}
