package libshell

import (
	"time"
)

type ShellHistory struct {
	ID        uint `gorm:"primarykey"`
	CreatedAt time.Time

	User    string `gorm:"size:256"`
	Content string `gorm:"size:4096"`
}
