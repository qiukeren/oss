package libcoreutil

import (
	log "github.com/sirupsen/logrus"
	"os/user"
)

func CoreUser(string) {
	u, err := user.Current()
	if err != nil {
		log.Println(err)
	} else {
		log.Printf("Uid %s Gid %s Username %s Name %s HomeDir %s", u.Uid, u.Gid, u.Username, u.Name, u.HomeDir)
	}
}
