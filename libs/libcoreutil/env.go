package libcoreutil

import (
	"github.com/olekukonko/tablewriter"
	"os"
	"strings"
)

func CoreEnv(string) {

	data := [][]string{}
	envs := os.Environ()
	for _, k := range envs {
		k := strings.Split(k, "=")[0]
		data = append(data, []string{k, os.Getenv(k)})
	}

	table := tablewriter.NewWriter(os.Stdout)

	table.SetAutoWrapText(true)
	table.SetAutoFormatHeaders(true)
	table.SetReflowDuringAutoWrap(true)

	table.SetHeader([]string{"Key", "Value"})
	table.SetColWidth(80)

	for _, v := range data {
		table.Append(v)
	}
	table.Render()

}
