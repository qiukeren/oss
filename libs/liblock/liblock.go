package liblock

import (
	log "github.com/sirupsen/logrus"
	"oss/libs/liblock/libfilelock"
)

func Lock(file string) {
	m, err := libfilelock.New(file)
	if err != nil {
		log.Println(err)
	} else {
		m.Lock() // Will block until lock can be acquired
	}
	m.Close()
}

func UnLock(file string) {
	m, err := libfilelock.New(file)
	if err != nil {
		log.Println(err)
	} else {
		m.Unlock()
	}
	m.Close()
}
