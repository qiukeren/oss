package libnetwork

import (
	"github.com/vishvananda/netlink"
	"oss/libs/libnetwork/networkmodel"
)

func LinkList() []networkmodel.LinkListItem {
	var items []networkmodel.LinkListItem

	x, _ := netlink.LinkList()
	for _, v := range x {
		items = append(items, networkmodel.LinkListItem{
			Name:         v.Attrs().Name,
			Type:         v.Type(),
			HardwareAddr: v.Attrs().HardwareAddr.String(),
		})
	}
	return items
}

//
//func LinkIp(name string) ([]string, error) {
//	l, err := netlink.LinkByName(name)
//	if err != nil {
//		return nil, err
//	}
//
//}
