package networkmodel

type LinkListItem struct {
	Name         string   `json:"name"`
	Type         string   `json:"type"`
	HardwareAddr string   `json:"hardwareAddr"`
	Ip           []string `json:"ip"`
}
